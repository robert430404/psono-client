#!/usr/bin/env bash
wget https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js -O src/common/data/js/lib/jquery.min.js
wget https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js -O src/common/data/js/lib/jquery-ui.min.js
wget https://raw.githubusercontent.com/angular-ui/bootstrap-bower/master/ui-bootstrap-tpls.min.js -O src/common/data/js/lib/ui-bootstrap-tpls.min.js
wget https://raw.githubusercontent.com/angular-ui/bootstrap/gh-pages/ui-bootstrap-2.5.0-csp.css -O src/common/data/css/lib/ui-bootstrap-csp.css
wget http://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js -O src/common/data/js/lib/datatables.min.js
wget https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css -O src/common/data/css/lib/datatables.min.css

wget https://code.angularjs.org/1.6.9/angular.min.js -O src/common/data/js/lib/angular.min.js
wget https://code.angularjs.org/1.6.9/angular-animate.min.js -O src/common/data/js/lib/angular-animate.min.js
wget https://code.angularjs.org/1.6.9/angular-route.min.js -O src/common/data/js/lib/angular-route.min.js
wget https://code.angularjs.org/1.6.9/angular-sanitize.min.js -O src/common/data/js/lib/angular-sanitize.min.js
wget https://code.angularjs.org/1.6.9/angular-touch.min.js -O src/common/data/js/lib/angular-touch.min.js
wget https://code.angularjs.org/1.6.9/angular-csp.css -O src/common/data/css/lib/angular-csp.css
wget https://code.angularjs.org/1.6.9/angular-mocks.js -O unittests/data/js/lib/angular-mocks.js

wget https://raw.githubusercontent.com/Kraku/angular-complexify/master/build/angular-complexify.min.js -O src/common/data/js/lib/angular-complexify.min.js
wget https://raw.githubusercontent.com/grevory/angular-local-storage/master/dist/angular-local-storage.min.js -O src/common/data/js/lib/angular-local-storage.min.js
wget https://raw.githubusercontent.com/techfort/LokiJS/master/build/lokijs.min.js -O src/common/data/js/lib/lokijs.min.js
wget https://raw.githubusercontent.com/RubaXa/Sortable/master/Sortable.min.js -O src/common/data/js/lib/sortable.min.js
wget https://raw.githubusercontent.com/jakiestfu/Snap.js/develop/snap.min.js -O src/common/data/js/lib/snap.min.js
wget https://raw.githubusercontent.com/emn178/js-sha512/master/build/sha512.min.js -O src/common/data/js/lib/sha512.min.js
wget https://raw.githubusercontent.com/emn178/js-sha256/master/build/sha256.min.js -O src/common/data/js/lib/sha256.min.js
wget https://raw.githubusercontent.com/emn178/js-sha1/master/build/sha1.min.js -O src/common/data/js/lib/sha1.min.js
wget https://raw.githubusercontent.com/3nsoft/ecma-nacl/master/dist/lib-browser/ecma-nacl.min.js -O src/common/data/js/lib/ecma-nacl.min.js
wget https://raw.githubusercontent.com/jackspirou/clientjs/master/dist/client.min.js -O src/common/data/js/lib/client.min.js

wget https://raw.githubusercontent.com/chieffancypants/angular-loading-bar/master/build/loading-bar.min.css -O src/common/data/css/lib/loading-bar.min.css
wget https://raw.githubusercontent.com/chieffancypants/angular-loading-bar/master/build/loading-bar.min.js -O src/common/data/js/lib/loading-bar.min.js
wget https://raw.githubusercontent.com/kazuhikoarase/qrcode-generator/master/js/qrcode.js -O src/common/data/js/lib/qrcode.min.js
wget https://raw.githubusercontent.com/mholt/PapaParse/master/papaparse.min.js -O src/common/data/js/lib/papaparse.min.js
wget https://raw.githubusercontent.com/NaturalIntelligence/fast-xml-parser/master/lib/parser.js -O src/common/data/js/lib/fast-xml-parser.js

wget https://raw.githubusercontent.com/ftlabs/fastclick/master/lib/fastclick.js -O src/common/data/js/lib/fastclick.js


wget https://raw.githubusercontent.com/chartjs/Chart.js/v2.7.1/dist/Chart.min.js -O src/common/data/js/lib/chart.min.js
wget https://raw.githubusercontent.com/jtblin/angular-chart.js/master/dist/angular-chart.min.js -O src/common/data/js/lib/angular-chart.min.js

wget https://raw.githubusercontent.com/openpgpjs/openpgpjs/master/dist/openpgp.min.js -O src/common/data/js/lib/openpgp.min.js
wget https://raw.githubusercontent.com/openpgpjs/openpgpjs/master/dist/openpgp.worker.min.js -O src/common/data/js/lib/openpgp.worker.min.js

